mod matrix_pat;
mod matrix_type;
mod quantity_mod;

use proc_macro::TokenStream;
use syn::parse_macro_input;

#[proc_macro]
pub fn make_matrix_type(input: TokenStream) -> TokenStream {
    matrix_type::make_matrix_type(parse_macro_input!(input as matrix_type::input::Input)).into()
}

#[proc_macro]
pub fn make_quantity_mod(input: TokenStream) -> TokenStream {
    quantity_mod::make_quantity_mod(parse_macro_input!(input as quantity_mod::input::Input)).into()
}

#[proc_macro_attribute]
#[cfg(feature = "matrix_pat")]
pub fn matrix_pat(_attr: TokenStream, input: TokenStream) -> TokenStream {
    matrix_pat::matrix_pat(parse_macro_input!(input as syn::Stmt))
        .unwrap_or_else(syn::Error::into_compile_error)
        .into()
}

use itertools::Itertools;
use proc_macro2::TokenStream;
use quote::{quote, ToTokens};
use syn::Path;

use self::input::{BinaryOp, CellSpec, Input, Spec, UnaryOp};

pub mod input;

pub fn make_matrix_type(input: Input) -> TokenStream {
    let Input { crate_, spec, .. } = input;

    realize_spec(&crate_, &spec)
}

fn realize_spec(crate_: &Path, spec: &Spec) -> TokenStream {
    match spec {
        Spec::Enumerated(spec) => combine_vertical(
            &crate_,
            &spec
                .iter()
                .map(|row| {
                    combine_horizontal(
                        &crate_,
                        &row.iter()
                            .map(|cell| realize_cell(&crate_, cell))
                            .collect_vec()[..],
                    )
                })
                .collect_vec()[..],
        ),
    }
}

fn realize_cell(crate_: &Path, cell: &CellSpec) -> TokenStream {
    match cell {
        CellSpec::TypeCell(ty) => ty.to_token_stream(),
        CellSpec::GroupCell(_, inner) => realize_spec(crate_, inner),
        CellSpec::UnaryCell(op, inner) => {
            let inner = realize_cell(crate_, inner);
            let values = match op {
                UnaryOp::Transpose(_) => quote!(TransposeValues),
                UnaryOp::Det(_) => quote!(DeterminantValues),
                UnaryOp::Inverse(_) => quote!(InverseValues),
            };
            quote!(#crate_::#values<#inner>)
        }
        CellSpec::BinaryCell(lhs, op, rhs) => {
            let lhs = realize_cell(crate_, lhs);
            let rhs = realize_cell(crate_, rhs);
            let values = match op {
                BinaryOp::Map(_) => quote!(MapValues),
                BinaryOp::Mul(_) => quote!(MulValues),
                BinaryOp::Div(_) => quote!(DivValues),
            };
            quote!(#crate_::#values<#lhs, #rhs>)
        }
    }
}

fn combine_horizontal(crate_: &Path, cells: &[TokenStream]) -> TokenStream {
    let head = &cells[0];
    let tail = &cells[1..];

    if tail.is_empty() {
        head.clone()
    } else {
        let combined_tail = combine_horizontal(crate_, tail);
        quote! {
            #crate_::Value<
                <#head as #crate_::HasUnderlyingType>::UnderlyingType,
                #crate_::HorizontalStackQuantities<
                    <#head as #crate_::ValueType<
                        { <#head as #crate_::Dimensioned>::ROWS },
                        { <#head as #crate_::Dimensioned>::COLUMNS },
                    >>::Q,
                    <#combined_tail as #crate_::ValueType<
                        { <#head as #crate_::Dimensioned>::ROWS },
                        { 0 #(+ <#tail as #crate_::Dimensioned>::COLUMNS)* },
                    >>::Q,
                    { <#head as #crate_::Dimensioned>::ROWS },
                    { <#head as #crate_::Dimensioned>::COLUMNS },
                    { 0 #(+ <#tail as #crate_::Dimensioned>::COLUMNS)* },
                >,
                { <#head as #crate_::Dimensioned>::ROWS },
                { <#head as #crate_::Dimensioned>::COLUMNS #(+ <#tail as #crate_::Dimensioned>::COLUMNS)* },
            >
        }
    }
}

fn combine_vertical(crate_: &Path, cells: &[TokenStream]) -> TokenStream {
    let head = &cells[0];
    let tail = &cells[1..];

    if tail.is_empty() {
        head.clone()
    } else {
        let combined_tail = combine_vertical(crate_, tail);
        quote! {
            #crate_::Value<
                <#head as #crate_::HasUnderlyingType>::UnderlyingType,
                #crate_::VerticalStackQuantities<
                    <#head as #crate_::ValueType<
                        { <#head as #crate_::Dimensioned>::ROWS },
                        { <#head as #crate_::Dimensioned>::COLUMNS },
                    >>::Q,
                    <#combined_tail as #crate_::ValueType<
                        { 0 #(+ <#tail as #crate_::Dimensioned>::ROWS)* },
                        { <#head as #crate_::Dimensioned>::COLUMNS },
                    >>::Q,
                    { <#head as #crate_::Dimensioned>::ROWS },
                    { 0 #(+ <#tail as #crate_::Dimensioned>::ROWS)* },
                    { <#head as #crate_::Dimensioned>::COLUMNS },
                >,
                { <#head as #crate_::Dimensioned>::ROWS #(+ <#tail as #crate_::Dimensioned>::ROWS)* },
                { <#head as #crate_::Dimensioned>::COLUMNS },
            >
        }
    }
}

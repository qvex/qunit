use syn::{
    bracketed,
    parse::{Parse, ParseStream},
    punctuated::Punctuated,
    token::Bracket,
    Path, Token, Type,
};

pub struct Input {
    pub crate_: Path,
    pub semi_token: Token![;],
    pub spec: Spec,
}

impl Parse for Input {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        Ok(Self {
            crate_: Path::parse(input)?,
            semi_token: Parse::parse(input)?,
            spec: Spec::parse(input)?,
        })
    }
}

pub enum Spec {
    Enumerated(Punctuated<Punctuated<CellSpec, Token![,]>, Token![;]>),
}

impl Parse for Spec {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        Ok(Self::Enumerated(Punctuated::parse_terminated_with(
            &input,
            Punctuated::parse_separated_nonempty,
        )?))
    }
}

pub enum CellSpec {
    TypeCell(Type),
    GroupCell(Bracket, Box<Spec>),
    UnaryCell(UnaryOp, Box<CellSpec>),
    BinaryCell(Box<CellSpec>, BinaryOp, Box<CellSpec>),
}

impl Parse for CellSpec {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        parse_cell(input, Precedence::Any)
    }
}

fn parse_cell(input: ParseStream, precedence: Precedence) -> syn::Result<CellSpec> {
    if precedence >= Precedence::Unary {
        return Ok(if input.peek(Bracket) {
            let content;
            CellSpec::GroupCell(bracketed!(content in input), Box::new(content.parse()?))
        } else if input.peek(Token![~]) {
            CellSpec::UnaryCell(
                UnaryOp::Transpose(input.parse()?),
                Box::new(parse_cell(input, Precedence::Unary)?),
            )
        } else if input.peek(Token![@]) {
            CellSpec::UnaryCell(
                UnaryOp::Det(input.parse()?),
                Box::new(parse_cell(input, Precedence::Unary)?),
            )
        } else if input.peek(Token![!]) {
            CellSpec::UnaryCell(
                UnaryOp::Inverse(input.parse()?),
                Box::new(parse_cell(input, Precedence::Unary)?),
            )
        } else {
            CellSpec::TypeCell(Type::parse(input)?)
        });
    }

    let mut lhs = parse_cell(input, Precedence::Unary)?;

    loop {
        lhs = if precedence < Precedence::Map && input.peek(Token![->]) {
            CellSpec::BinaryCell(
                Box::new(lhs),
                BinaryOp::Map(input.parse()?),
                Box::new(parse_cell(input, Precedence::Map)?),
            )
        } else if precedence < Precedence::MulDiv && input.peek(Token![*]) {
            CellSpec::BinaryCell(
                Box::new(lhs),
                BinaryOp::Mul(input.parse()?),
                Box::new(parse_cell(input, Precedence::MulDiv)?),
            )
        } else if precedence < Precedence::MulDiv && input.peek(Token![/]) {
            CellSpec::BinaryCell(
                Box::new(lhs),
                BinaryOp::Div(input.parse()?),
                Box::new(parse_cell(input, Precedence::MulDiv)?),
            )
        } else {
            return Ok(lhs);
        }
    }
}

pub enum UnaryOp {
    Transpose(Token![~]),
    Det(Token![@]),
    Inverse(Token![!]),
}

pub enum BinaryOp {
    Map(Token![->]),
    Mul(Token![*]),
    Div(Token![/]),
}

#[derive(PartialEq, Eq, PartialOrd, Ord)]
enum Precedence {
    Any,
    Map,
    MulDiv,
    Unary,
}

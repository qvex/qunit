use syn::{
    bracketed,
    parse::{Parse, ParseStream},
    punctuated::Punctuated,
    token::Bracket,
    Attribute, Pat, Token, Type,
};

pub enum Pattern {
    Enumerated(Punctuated<Punctuated<CellPattern, Token![,]>, Token![;]>),
}

impl Parse for Pattern {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        Ok(Self::Enumerated(Punctuated::parse_terminated_with(
            &input,
            Punctuated::parse_separated_nonempty,
        )?))
    }
}

pub enum CellPattern {
    AtomCell(Pat, Option<Box<(Token![:], Type)>>),
    GroupCell(Vec<Attribute>, Bracket, Box<Pattern>),
}

impl Parse for CellPattern {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        Ok(if input.peek(Bracket) {
            let content;
            Self::GroupCell(
                Attribute::parse_outer(input)?,
                bracketed!(content in input),
                Box::new(content.parse()?),
            )
        } else {
            Self::AtomCell(
                Pat::parse_multi(input)?,
                if input.peek(Token![:]) {
                    Some(Box::new((input.parse()?, input.parse()?)))
                } else {
                    None
                },
            )
        })
    }
}

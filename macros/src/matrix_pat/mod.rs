#![cfg(feature = "matrix_pat")]

pub mod pat;

use proc_macro2::{Ident, Span, TokenStream};
use quote::quote;
use syn::{
    parse2, parse_quote, spanned::Spanned, Error, Local, LocalInit, Pat, PatMacro, Stmt, Type,
};

pub fn matrix_pat(statement: Stmt) -> syn::Result<TokenStream> {
    if let Stmt::Local(Local {
        attrs,
        let_token,
        pat,
        init,
        semi_token,
    }) = statement
    {
        if let Pat::Macro(PatMacro {
            attrs: pat_attrs,
            mac,
        }) = pat
        {
            if mac
                .path
                .segments
                .last()
                .map(|s| s.ident.to_string() == "matrix")
                != Some(true)
            {
                return Err(Error::new(mac.path.span(), "expected `matrix!` macro"));
            }

            let pat: pat::Pattern = if let syn::MacroDelimiter::Bracket(_) = mac.delimiter {
                parse2(mac.tokens)?
            } else {
                return Err(Error::new(
                    mac.span(),
                    "expected bracketed macro invocation",
                ));
            };

            let LocalInit {
                eq_token,
                expr,
                diverge,
            } = init.ok_or_else(|| Error::new(semi_token.span(), "expected initializer"))?;

            let diverge =
                diverge.map_or_else(TokenStream::new, |(else_, expr)| quote!(#else_ #expr));

            let mut cell_pats = Vec::new();
            let mut cell_types = Vec::new();
            let mut cell_names = Vec::new();

            fn visit_pattern(
                rhs: TokenStream,
                pat: pat::Pattern,
                cell_pats: &mut Vec<Pat>,
                cell_types: &mut Vec<Type>,
                cell_names: &mut Vec<Ident>,
            ) -> TokenStream {
                let visitor = Ident::new("__visitor", Span::call_site());
                match pat {
                    pat::Pattern::Enumerated(rows) => {
                        let rows = rows.into_iter().map(|row| {
                            let cells = row.into_iter().map(|cell| {
                                let name = Ident::new(
                                    &format!("__cell_{}", cell_names.len()),
                                    Span::call_site(),
                                );
                                cell_names.push(name.clone());
                                match cell {
                                    pat::CellPattern::AtomCell(pat, ty) => {
                                        cell_pats.push(pat);
                                        cell_types.push(if let Some(ty) = ty {
                                            ty.1
                                        } else {
                                            parse_quote!(_)
                                        });
                                        quote! {
                                            let #name = ::qunit::visitors::Visitor::visit(&mut #visitor);
                                        }
                                    }
                                    pat::CellPattern::GroupCell(_, _, _) => todo!(),
                                }
                            });
                            quote! {
                                #(#cells)*
                                ::qunit::visitors::Visitor::eol(&mut #visitor);
                            }
                        });
                        quote! {
                            let mut #visitor = ::qunit::Value::visit(&#rhs);
                            #(#rows)*
                            ::qunit::visitors::Visitor::eof(&mut #visitor);
                        }
                    }
                }
            }

            let cell_process = visit_pattern(
                quote!(#expr),
                pat,
                &mut cell_pats,
                &mut cell_types,
                &mut cell_names,
            );

            Ok(quote! {
                #(#attrs)*
                #let_token #(#pat_attrs)* (#(#cell_pats,)*): (#(#cell_types,)*) #eq_token {
                    #cell_process
                    (#(#cell_names,)*)
                } #diverge #semi_token
            })
        } else {
            Err(Error::new(pat.span(), "expected macro invocation"))
        }
    } else {
        Err(Error::new(statement.span(), "expected `let` binding"))
    }
}

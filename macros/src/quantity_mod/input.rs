use syn::{
    parse::{Parse, ParseStream},
    punctuated::Punctuated,
    Expr, Ident, LitFloat, Path, Token,
};

pub struct Input {
    pub crate_: Path,
    pub name: Ident,
    pub quantity: Expr,
    pub semi: Token![;],
    pub units: Punctuated<Unit, Token![,]>,
}

impl Parse for Input {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        Ok(Self {
            crate_: input.parse()?,
            name: input.parse()?,
            quantity: input.parse()?,
            semi: input.parse()?,
            units: Punctuated::parse_terminated(input)?,
        })
    }
}

pub struct Unit {
    pub name: Ident,
    pub eq: Token![=],
    pub value: LitFloat,
}

impl Parse for Unit {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        Ok(Self {
            name: input.parse()?,
            eq: input.parse()?,
            value: input.parse()?,
        })
    }
}

use convert_case::{Case, Casing};
use itertools::Itertools;
use proc_macro2::TokenStream;
use quote::quote;
use syn::Ident;

use self::input::{Input, Unit};

pub mod input;

pub fn make_quantity_mod(input: Input) -> TokenStream {
    let Input {
        crate_,
        name,
        quantity,
        units,
        ..
    } = input;

    let const_names = units
        .iter()
        .map(|u| Ident::new(format!("UNIT_{}", u.name).as_str(), u.name.span()))
        .collect_vec();

    let constants = units.iter().enumerate().map(|(i, Unit { value, .. })| {
        let const_name = &const_names[i];
        quote! {
            pub const #const_name: Self = Self::new_si([[#value]]);
        }
    });

    let type_ = Ident::new(name.to_string().to_case(Case::Pascal).as_str(), name.span());
    let trait_ = Ident::new(format!("{}Ext", type_).as_str(), name.span());

    let units_in = units.iter().enumerate().map(|(i, Unit { name, .. })| {
        let const_name = &const_names[i];
        quote! {
            fn #name(self) -> #type_<Self> {
                #type_::#const_name.convert::<Self>() * self
            }
        }
    });

    let units_out = units.iter().enumerate().map(|(i, u)| {
        let to = Ident::new(format!("to_{}", u.name).as_str(), u.name.span());
        let const_name = &const_names[i];
        quote! {
            pub fn #to(self) -> T {
                (self / #type_::#const_name.convert()).value[0]
            }
        }
    });

    let units_out_vec = units.iter().enumerate().map(|(i, u)| {
        let to = Ident::new(format!("to_{}_vec", u.name).as_str(), u.name.span());
        let const_name = &const_names[i];
        quote! {
            pub fn #to(self) -> #crate_::nalgebra::SMatrix<T, R, C> {
                (self / #type_::#const_name.convert()).value
            }
        }
    });

    quote! {
        #[allow(non_snake_case, non_upper_case_globals)]
        pub mod #name {
            pub type #type_<T = #crate_::DefaultField, const R: usize = 1, const C: usize = 1> =
                #crate_::Value<T, #crate_::UniformQuantities<{ #quantity }, R, C>, R, C>;

            impl #type_ {
                #(#constants)*
            }

            pub trait #trait_: #crate_::nalgebra::ComplexField + ::core::marker::Copy + #crate_::simba::scalar::SupersetOf<#crate_::DefaultField>
            where
                #crate_::DefaultField: #crate_::simba::scalar::SupersetOf<Self> {
                #(#units_in)*
            }

            impl<T: #crate_::nalgebra::ComplexField + ::core::marker::Copy + #crate_::simba::scalar::SupersetOf<#crate_::DefaultField>> #trait_ for T
            where
                #crate_::DefaultField: #crate_::simba::scalar::SupersetOf<Self> {}

            impl<T: #crate_::nalgebra::ComplexField + ::core::marker::Copy + #crate_::simba::scalar::SupersetOf<#crate_::DefaultField>> #type_<T> {
                #(#units_out)*
            }

            impl<
                T: #crate_::nalgebra::ComplexField + ::core::marker::Copy + #crate_::simba::scalar::SupersetOf<#crate_::DefaultField>,
                const R: usize,
                const C: usize,
            > #type_<T, R, C> {
                #(#units_out_vec)*
            }
        }
    }
}

#![feature(generic_const_exprs)]

use qunit::{
    angle::AngleExt,
    length::{Length, LengthExt},
    matrix,
    time::TimeExt,
    Matrix,
};

type Column = Matrix![Length; Length];
type Area = Matrix![Length * Length];
type Full = Matrix![Column, Column];
type ColumnMap = Matrix![Column -> Column];

fn main() {
    //     let test = 1.0.m();

    //     println!("before = {:?}", test);

    //     let serialized = serde_json::to_string(&test).unwrap();
    //     println!("serialized = {}", serialized);

    //     let deserialized: Length = serde_json::from_str(&serialized).unwrap();
    //     println!("deserialized = {:?}", deserialized);
    let l1 = 1.0.m();
    let l2 = 3.2.m();
    let test = matrix![1.0.deg(); 2.0.deg()];
    let test2 = l1 * TimeExt::min(1.0);
    let l3 = l1 + l2;
    let area: Area = l1 * l2;
    let m = matrix![l1, l2; l3 * l3, area];
    println!("{:?}", m);
    println!("{:?}", m * matrix![l1; l2]);
    println!("{:?}", l2 * m);
    println!("{:?}", m * l2);
    println!("{:?}", m / l2);

    let row_matrix: Matrix![Length, Length];
    row_matrix = matrix![l1, l2];
    println!("{:?}", row_matrix);
    let row_matrix_serialized = serde_json::to_string(&row_matrix).unwrap();
    println!("serialized row: {}", row_matrix_serialized);
    let row_matrix_deserialized: Matrix![Length, Length] =
        serde_json::from_str(&row_matrix_serialized).unwrap();
    println!("deserialized row matrix: {:?}", row_matrix_deserialized);

    let column_matrix: Matrix![Length; Length];
    column_matrix = matrix![l1; l2];
    println!("{:?}", column_matrix);

    let full_matrix: Full;
    full_matrix = matrix![l1, l2; l2, l1].as_equiv();
    println!("full_matrix = {:?}", full_matrix);
    let det: Matrix![@Full] = full_matrix.determinant();
    println!("det = {:?}", det);
    let transpose: Matrix![~Full] = full_matrix.transpose();
    println!("transpose = {:?}", transpose);
    let full_inv: Option<Matrix![!Full]> = full_matrix.try_inverse();
    println!("inv = {:?}", full_inv);
    let dfull: Matrix![Full, Full; Full, Full] =
        matrix![full_matrix, full_matrix; full_matrix, full_matrix];
    let dfull_det: Matrix![@[Full, Full; Full, Full]] = dfull.determinant();
    println!("det {:?} = {:?}", dfull, dfull_det);

    let mm = matrix![m, m; m, m];
    println!("{:?}", mm);
    println!("{:?}", mm.index::<{ 1..3 }, { 1..3 }>());

    let det = m.determinant();
    println!("{:?}", det);

    println!("{:?}", m.try_inverse());
    println!("{:?}", mm.try_inverse());
    println!("{:?}", -mm);
}

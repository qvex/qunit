#![no_std]
#![feature(adt_const_params)]
#![feature(const_maybe_uninit_uninit_array)]
#![feature(const_trait_impl)]
#![feature(const_type_id)]
#![feature(generic_const_exprs)]
#![feature(maybe_uninit_uninit_array)]
#![feature(maybe_uninit_uninit_array_transpose)]
#![feature(proc_macro_hygiene)]
#![allow(incomplete_features)]
#![allow(type_alias_bounds)]
#![cfg_attr(feature = "math", feature(array_zip))]
#![cfg_attr(docsrs, feature(doc_cfg))]

pub mod coordinates;
mod math;
mod util;
pub mod visitors;

extern crate self as qunit;

use core::{
    cmp::Ordering,
    fmt::{self, Debug},
    iter::Sum,
    marker::PhantomData,
    mem::ManuallyDrop,
    ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Range, Sub, SubAssign},
    time::{Duration, TryFromFloatSecsError},
};

use nalgebra::{convert, ArrayStorage, ComplexField, Const, DimMin, Matrix1, SMatrix};
use num_traits::{Signed, Zero};
use simba::scalar::{SubsetOf, SupersetOf};

#[doc(hidden)]
pub use qunit_macros as macros;

#[doc(hidden)]
pub use nalgebra;

#[doc(hidden)]
pub use simba;

#[cfg(feature = "serde")]
use serde::{de::Visitor, ser::SerializeTuple, Deserialize, Deserializer, Serialize, Serializer};

#[cfg(feature = "matrix_pat")]
#[cfg_attr(docsrs, doc(cfg(feature = "matrix_pat")))]
pub use macros::matrix_pat;

#[cfg(feature = "default-f32")]
#[cfg_attr(docsrs, doc(cfg(feature = "default-f32")))]
pub type DefaultField = f32;

#[cfg(not(feature = "default-f32"))]
#[cfg_attr(docsrs, doc(cfg(not(feature = "default-f32"))))]
pub type DefaultField = f64;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct SIQuantity {
    pub time: isize,
    pub length: isize,
    pub mass: isize,
    pub electric_current: isize,
    pub thermodynamic_temperature: isize,
    pub amount_of_substance: isize,
    pub luminous_intensity: isize,
}

impl SIQuantity {
    pub const fn is_equal(self, rhs: Self) -> bool {
        self.time == rhs.time
            && self.length == rhs.length
            && self.mass == rhs.mass
            && self.electric_current == rhs.electric_current
            && self.thermodynamic_temperature == rhs.thermodynamic_temperature
            && self.amount_of_substance == rhs.amount_of_substance
            && self.luminous_intensity == rhs.luminous_intensity
    }

    pub const fn mul(self, rhs: Self) -> Self {
        Self {
            time: self.time + rhs.time,
            length: self.length + rhs.length,
            mass: self.mass + rhs.mass,
            electric_current: self.electric_current + rhs.electric_current,
            thermodynamic_temperature: self.thermodynamic_temperature
                + rhs.thermodynamic_temperature,
            amount_of_substance: self.amount_of_substance + rhs.amount_of_substance,
            luminous_intensity: self.luminous_intensity + rhs.luminous_intensity,
        }
    }

    pub const fn div(self, rhs: Self) -> Self {
        Self {
            time: self.time - rhs.time,
            length: self.length - rhs.length,
            mass: self.mass - rhs.mass,
            electric_current: self.electric_current - rhs.electric_current,
            thermodynamic_temperature: self.thermodynamic_temperature
                - rhs.thermodynamic_temperature,
            amount_of_substance: self.amount_of_substance - rhs.amount_of_substance,
            luminous_intensity: self.luminous_intensity - rhs.luminous_intensity,
        }
    }

    pub const fn sqrt(self) -> Self {
        Self {
            time: util::idiv(self.time, 2),
            length: util::idiv(self.length, 2),
            mass: util::idiv(self.mass, 2),
            electric_current: util::idiv(self.electric_current, 2),
            thermodynamic_temperature: util::idiv(self.thermodynamic_temperature, 2),
            amount_of_substance: util::idiv(self.amount_of_substance, 2),
            luminous_intensity: util::idiv(self.luminous_intensity, 2),
        }
    }
}

impl Mul for SIQuantity {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        self.mul(rhs)
    }
}

impl Div for SIQuantity {
    type Output = Self;

    fn div(self, rhs: Self) -> Self::Output {
        self.div(rhs)
    }
}

#[macro_export]
macro_rules! quantity {
    [$t:expr, $l:expr, $m:expr, $ec:expr, $tt:expr, $as:expr, $li:expr] => {
        $crate::SIQuantity {
            time: $t,
            length: $l,
            mass: $m,
            electric_current: $ec,
            thermodynamic_temperature: $tt,
            amount_of_substance: $as,
            luminous_intensity: $li,
        }
    };
}

pub const RATIO: SIQuantity = quantity![0, 0, 0, 0, 0, 0, 0];
pub const TIME: SIQuantity = quantity![1, 0, 0, 0, 0, 0, 0];
pub const LENGTH: SIQuantity = quantity![0, 1, 0, 0, 0, 0, 0];
pub const MASS: SIQuantity = quantity![0, 0, 1, 0, 0, 0, 0];
pub const ELECTRIC_CURRENT: SIQuantity = quantity![0, 0, 0, 1, 0, 0, 0];
pub const THERMODYNAMIC_TEMPERATURE: SIQuantity = quantity![0, 0, 0, 0, 1, 0, 0];
pub const AMOUNT_OF_SUBSTANCE: SIQuantity = quantity![0, 0, 0, 0, 0, 1, 0];
pub const LUMINOUS_INTENSITY: SIQuantity = quantity![0, 0, 0, 0, 0, 0, 1];

macro_rules! quantity_mod {
    ($($content:tt)*) => {$crate::macros::make_quantity_mod! { $crate $($content)* }};
}

quantity_mod!(ratio super::RATIO; percent = 0.01);
quantity_mod!(angle super::RATIO; rad = 1.0, deg = 0.017453292519943, cdeg = 0.00017453292519943, rev = 6.2831853071796);
quantity_mod!(time super::TIME; s = 1.0, ms = 0.001, us = 0.000001, ns = 0.000000001, min = 60.0);
quantity_mod!(length super::LENGTH; m = 1.0, cm = 0.01, mm = 0.001, um = 0.000001, in_ = 0.0254);
quantity_mod!(mass super::MASS; kg = 1.0);
quantity_mod!(current super::ELECTRIC_CURRENT; A = 1.0, mA = 0.001);
quantity_mod!(temperature super::THERMODYNAMIC_TEMPERATURE; K = 1.0);
quantity_mod!(area super::LENGTH.mul(super::LENGTH); m2 = 1.0, in2 = 0.00064516);
quantity_mod!(voltage quantity![-3, 2, 1, -1, 0, 0, 0]; V = 1.0, mV = 0.001);
quantity_mod!(
    angular_velocity super::quantity![-1, 0, 0, 0, 0, 0, 0];
    radps = 1.0,
    degps = 0.017453292519943,
    cdegps = 0.00017453292519943,
    revps = 6.2831853071796,
    rpm = 0.1047198,
);
quantity_mod!(velocity quantity![-1, 1, 0, 0, 0, 0, 0]; mps = 1.0, in_ps = 0.0254);
quantity_mod!(power quantity![-3, 2, 1, 0, 0, 0, 0]; W = 1.0, mW = 0.001);
quantity_mod!(torque quantity![-2, 2, 1, 0, 0, 0, 0]; Nm = 1.0);

impl<T: ComplexField + Copy + SupersetOf<f64>> From<Duration> for time::Time<T> {
    fn from(value: Duration) -> Self {
        Value::new_si([[convert(value.as_secs_f64())]])
    }
}

impl<T: ComplexField + SupersetOf<DefaultField> + Copy> TryFrom<time::Time<T>> for Duration
where
    f64: SupersetOf<T>,
{
    type Error = TryFromFloatSecsError;

    fn try_from(value: time::Time<T>) -> Result<Self, Self::Error> {
        Duration::try_from_secs_f64(convert(time::Time::to_s(value)))
    }
}

pub trait Quantities<const R: usize, const C: usize>: 'static {
    const QUANTITIES: [[SIQuantity; C]; R];
}

pub const fn equal_quantities<
    Q1: Quantities<R, C>,
    Q2: Quantities<R, C>,
    const R: usize,
    const C: usize,
>() -> usize {
    let mut i = 0;
    while i < R {
        let mut j = 0;
        while j < C {
            assert!(Q1::QUANTITIES[i][j].is_equal(Q2::QUANTITIES[i][j]));
            j += 1;
        }
        i += 1;
    }
    0
}

pub trait HasUnderlyingType {
    type UnderlyingType;
}

pub trait HasQuantities {
    const QUANTITIES: &'static [&'static [SIQuantity]];
}

pub trait ValueType<const R: usize, const C: usize>: HasUnderlyingType
where
    Self::UnderlyingType: ComplexField,
{
    type Q: Quantities<R, C>;
}

pub enum UniformQuantities<const Q: SIQuantity, const R: usize, const C: usize> {}

impl<const Q: SIQuantity, const R: usize, const C: usize> Quantities<R, C>
    for UniformQuantities<Q, R, C>
{
    const QUANTITIES: [[SIQuantity; C]; R] = [[Q; C]; R];
}

pub enum MatrixQuantities<const Q: &'static [&'static [SIQuantity]]> {}

const fn cols(data: &[&[SIQuantity]]) -> usize {
    data[0].len()
}

const fn quantities<const R: usize, const C: usize>(
    data: &[&[SIQuantity]],
) -> [[SIQuantity; C]; R] {
    assert!(data.len() == R);
    let mut result = [[RATIO; C]; R];
    let mut i = 0;
    while i < R {
        assert!(data[i].len() == C);
        let mut j = 0;
        while j < C {
            result[i][j] = data[i][j];
            j += 1;
        }
        i += 1;
    }
    result
}

impl<const Q: &'static [&'static [SIQuantity]]> Quantities<{ Q.len() }, { cols(Q) }>
    for MatrixQuantities<Q>
{
    const QUANTITIES: [[SIQuantity; cols(Q)]; Q.len()] = quantities(Q);
}

/// Identifies a matrix type given a specification.
///
/// The format of the specification is a MATLAB-style grid of cells (i.e., cells
/// within a row are comma-separated, rows are semicolon-separated). Each cell
/// is itself a matrix type; it can have any dimensions, as long as all cells in
/// the same row have the same number of rows, and all rows have the same total
/// number of columns.
///
/// Within a cell, types may also be combined using unary operators `~`, `@`,
/// and `!` (for transpose, determinant, and inverse, respectively), and binary
/// operators `->`, `*`, and `/` (for linear mapping, multiplication, and
/// division, respectively). Square brackets may also be used to recursively
/// embed another specification; this can in turn contain its own grid of cells.
///
/// # Example
/// ```
/// # use qunit::{*, length::*};
/// type Point2D = Matrix![Length<f64>, Length<f64>]; // Equivalent to Length<f64, 2, 1>.
/// type Covariance2D = Matrix![Point2D * ~Point2D];
/// ```
#[macro_export]
macro_rules! Matrix {
    [$($contents:tt)*] => {$crate::macros::make_matrix_type![$crate; $($contents)*]};
}

/// Constructs a matrix from smaller matrix values.
///
/// MATLAB-style syntax is used to combine cells (comma-separated) into rows and
/// rows (semicolon-separated) into matrices.
#[macro_export]
macro_rules! matrix {
    // Single cell.
    [$cell:expr] => {$cell};
    // Single row.
    [$head:expr, $($tail:expr),+ $(;)?] => {$head $(.append_right($tail))+};
    // Several rows.
    [$($head:expr),+; $($($tail:expr),+);+] => {{
        let head = $crate::matrix![$($head),+];
        let tail = $crate::matrix![$($($tail),+);+];
        head.append_below(tail)
    }};
}

const fn check_mul_quantities<
    const R1: usize,
    const C1: usize,
    const R2: usize,
    const C2: usize,
>(
    lhs: [[SIQuantity; C1]; R1],
    rhs: [[SIQuantity; C2]; R2],
) -> usize {
    if R1 == 1 && C1 == 1 || R2 == 1 && C2 == 1 {
        return 0;
    }

    assert!(R2 == C1);
    let mut i = 0;
    while i < R1 {
        let mut k = 0;
        while k < C2 {
            let q = lhs[i][0].mul(rhs[0][k]);
            let mut j = 1;
            while j < C1 {
                assert!(lhs[i][j].mul(rhs[j][k]).is_equal(q));
                j += 1;
            }
            k += 1;
        }
        i += 1;
    }
    0
}

pub const fn mul_rows(r1: usize, c1: usize, r2: usize, _c2: usize) -> usize {
    if r1 == 1 && c1 == 1 {
        r2
    } else {
        r1
    }
}

pub const fn mul_cols(_r1: usize, c1: usize, r2: usize, c2: usize) -> usize {
    if r2 == 1 && c2 == 1 {
        c1
    } else {
        c2
    }
}

pub struct MulQuantities<
    Q1: Quantities<R1, C1>,
    Q2: Quantities<R2, C2>,
    const R1: usize,
    const C1: usize,
    const R2: usize,
    const C2: usize,
>(PhantomData<(Q1, Q2)>);

impl<
        Q1: Quantities<R1, C1>,
        Q2: Quantities<R2, C2>,
        const R1: usize,
        const C1: usize,
        const R2: usize,
        const C2: usize,
    > Quantities<{ mul_rows(R1, C1, R2, C2) }, { mul_cols(R1, C1, R2, C2) }>
    for MulQuantities<Q1, Q2, R1, C1, R2, C2>
where
    [(); check_mul_quantities(Q1::QUANTITIES, Q2::QUANTITIES)]:,
{
    const QUANTITIES: [[SIQuantity; mul_cols(R1, C1, R2, C2)]; mul_rows(R1, C1, R2, C2)] = {
        let lhs = Q1::QUANTITIES;
        let rhs = Q2::QUANTITIES;

        let mut result = [[RATIO; mul_cols(R1, C1, R2, C2)]; mul_rows(R1, C1, R2, C2)];
        let mut i = 0;
        while i < result.len() {
            let mut j = 0;
            while j < result[i].len() {
                result[i][j] = if R1 == 1 && C1 == 1 {
                    lhs[0][0].mul(rhs[i][j])
                } else if R2 == 1 && C2 == 1 {
                    lhs[i][j].mul(rhs[0][0])
                } else {
                    lhs[i][0].mul(rhs[0][j])
                };
                j += 1;
            }
            i += 1;
        }
        result
    };
}

pub type MulValues<V1: Dimensioned, V2: Dimensioned> = Value<
    <V1 as HasUnderlyingType>::UnderlyingType,
    MulQuantities<
        <V1 as ValueType<{ <V1 as Dimensioned>::ROWS }, { <V1 as Dimensioned>::COLUMNS }>>::Q,
        <V2 as ValueType<{ <V2 as Dimensioned>::ROWS }, { <V2 as Dimensioned>::COLUMNS }>>::Q,
        { <V1 as Dimensioned>::ROWS },
        { <V1 as Dimensioned>::COLUMNS },
        { <V2 as Dimensioned>::ROWS },
        { <V2 as Dimensioned>::COLUMNS },
    >,
    {
        mul_rows(
            <V1 as Dimensioned>::ROWS,
            <V1 as Dimensioned>::COLUMNS,
            <V2 as Dimensioned>::ROWS,
            <V2 as Dimensioned>::COLUMNS,
        )
    },
    {
        mul_cols(
            <V1 as Dimensioned>::ROWS,
            <V1 as Dimensioned>::COLUMNS,
            <V2 as Dimensioned>::ROWS,
            <V2 as Dimensioned>::COLUMNS,
        )
    },
>;

pub struct DivQuantities<Q1: Quantities<R, C>, Q2: Quantities<1, 1>, const R: usize, const C: usize>(
    PhantomData<(Q1, Q2)>,
);

impl<Q1: Quantities<R, C>, Q2: Quantities<1, 1>, const R: usize, const C: usize> Quantities<R, C>
    for DivQuantities<Q1, Q2, R, C>
{
    const QUANTITIES: [[SIQuantity; C]; R] = {
        let mut result = Q1::QUANTITIES;
        let rhs = Q2::QUANTITIES[0][0];

        let mut i = 0;
        while i < R {
            let mut j = 0;
            while j < C {
                result[i][j] = result[i][j].div(rhs);
                j += 1;
            }
            i += 1;
        }

        result
    };
}

pub type DivValues<V1: Dimensioned, V2: Dimensioned> = Value<
    <V1 as HasUnderlyingType>::UnderlyingType,
    DivQuantities<
        <V1 as ValueType<{ <V1 as Dimensioned>::ROWS }, { <V1 as Dimensioned>::COLUMNS }>>::Q,
        <V2 as ValueType<1, 1>>::Q,
        { <V1 as Dimensioned>::ROWS },
        { <V1 as Dimensioned>::COLUMNS },
    >,
    { <V1 as Dimensioned>::ROWS },
    { <V1 as Dimensioned>::COLUMNS },
>;

const fn check_map_quantities<const R1: usize, const R2: usize, const C: usize>(
    domain: [[SIQuantity; C]; R1],
    codomain: [[SIQuantity; C]; R2],
) -> usize {
    check_mul_quantities(construct_map_quantities(domain, codomain), domain)
}

const fn construct_map_quantities<const R1: usize, const R2: usize, const C: usize>(
    domain: [[SIQuantity; C]; R1],
    codomain: [[SIQuantity; C]; R2],
) -> [[SIQuantity; R1]; R2] {
    let mut result = [[RATIO; R1]; R2];
    let mut i = 0;
    while i < R2 {
        let mut j = 0;
        while j < R1 {
            result[i][j] = codomain[i][0].div(domain[j][0]);
            j += 1;
        }
        i += 1;
    }
    result
}

pub struct MapQuantities<
    Q1: Quantities<R1, C>,
    Q2: Quantities<R2, C>,
    const R1: usize,
    const R2: usize,
    const C: usize,
>(PhantomData<(Q1, Q2)>);

impl<
        Q1: Quantities<R1, C>,
        Q2: Quantities<R2, C>,
        const R1: usize,
        const R2: usize,
        const C: usize,
    > Quantities<R2, R1> for MapQuantities<Q1, Q2, R1, R2, C>
where
    [(); check_map_quantities(Q1::QUANTITIES, Q2::QUANTITIES)]:,
{
    const QUANTITIES: [[SIQuantity; R1]; R2] =
        construct_map_quantities(Q1::QUANTITIES, Q2::QUANTITIES);
}

pub type MapValues<V1: Dimensioned, V2: Dimensioned> = Value<
    <V1 as HasUnderlyingType>::UnderlyingType,
    MapQuantities<
        <V1 as ValueType<{ <V1 as Dimensioned>::ROWS }, { <V1 as Dimensioned>::COLUMNS }>>::Q,
        <V2 as ValueType<{ <V2 as Dimensioned>::ROWS }, { <V2 as Dimensioned>::COLUMNS }>>::Q,
        { <V1 as Dimensioned>::ROWS },
        { <V2 as Dimensioned>::ROWS },
        { <V1 as Dimensioned>::COLUMNS },
    >,
    { <V2 as Dimensioned>::ROWS },
    { <V1 as Dimensioned>::ROWS },
>;

pub struct TransposeQuantities<Q: Quantities<R, C>, const R: usize, const C: usize>(PhantomData<Q>);

impl<Q: Quantities<R, C>, const R: usize, const C: usize> Quantities<C, R>
    for TransposeQuantities<Q, R, C>
{
    const QUANTITIES: [[SIQuantity; R]; C] = util::transpose_arrays(Q::QUANTITIES);
}

pub type TransposeValues<V: Dimensioned> = Value<
    <V as HasUnderlyingType>::UnderlyingType,
    TransposeQuantities<
        <V as ValueType<{ <V as Dimensioned>::ROWS }, { <V as Dimensioned>::COLUMNS }>>::Q,
        { <V as Dimensioned>::ROWS },
        { <V as Dimensioned>::COLUMNS },
    >,
    { <V as Dimensioned>::COLUMNS },
    { <V as Dimensioned>::ROWS },
>;

pub const fn check_determinant_quantities<const N: usize>(
    quantities: [[SIQuantity; N]; N],
) -> usize {
    let mut test_quantities = [[RATIO; 1]; N];
    let mut i = 0;
    while i < N {
        test_quantities[i][0] = RATIO.div(quantities[0][i]);
        i += 1;
    }
    check_mul_quantities(quantities, test_quantities)
}

const fn determinant_quantity_unchecked<const N: usize>(
    quantities: [[SIQuantity; N]; N],
) -> SIQuantity {
    let mut quantity = RATIO;
    let mut i = 0;
    while i < N {
        quantity = quantity.mul(quantities[i][i]);
        i += 1;
    }
    quantity
}

pub struct DeterminantQuantities<Q: Quantities<N, N>, const N: usize>(PhantomData<Q>);

impl<Q: Quantities<N, N>, const N: usize> Quantities<1, 1> for DeterminantQuantities<Q, N>
where
    [(); check_determinant_quantities(Q::QUANTITIES)]:,
{
    const QUANTITIES: [[SIQuantity; 1]; 1] = [[determinant_quantity_unchecked(Q::QUANTITIES)]];
}

pub type DeterminantValues<V: Dimensioned> = Value<
    <V as HasUnderlyingType>::UnderlyingType,
    DeterminantQuantities<
        <V as ValueType<{ <V as Dimensioned>::ROWS }, { <V as Dimensioned>::COLUMNS }>>::Q,
        { <V as Dimensioned>::ROWS },
    >,
    1,
    1,
>;

pub struct AdjugateQuantities<Q: Quantities<N, N>, const N: usize>(PhantomData<Q>);

impl<Q: Quantities<N, N>, const N: usize> Quantities<N, N> for AdjugateQuantities<Q, N>
where
    [(); check_determinant_quantities(Q::QUANTITIES)]:,
{
    const QUANTITIES: [[SIQuantity; N]; N] = {
        let det = determinant_quantity_unchecked(Q::QUANTITIES);
        let mut quantities = [[RATIO; N]; N];
        let mut i = 0;
        while i < N {
            let mut j = 0;
            while j < N {
                quantities[i][j] = det.div(Q::QUANTITIES[i][j]);
                j += 1;
            }
            i += 1;
        }
        quantities
    };
}

pub type InverseQuantities<Q, const N: usize> = DivQuantities<
    TransposeQuantities<AdjugateQuantities<Q, N>, N, N>,
    DeterminantQuantities<Q, N>,
    N,
    N,
>;

pub type InverseValues<V: Dimensioned> = Value<
    <V as HasUnderlyingType>::UnderlyingType,
    InverseQuantities<
        <V as ValueType<{ <V as Dimensioned>::ROWS }, { <V as Dimensioned>::COLUMNS }>>::Q,
        { <V as Dimensioned>::ROWS },
    >,
    { <V as Dimensioned>::ROWS },
    { <V as Dimensioned>::COLUMNS },
>;

pub struct HorizontalStackQuantities<
    Q1: Quantities<R, C1>,
    Q2: Quantities<R, C2>,
    const R: usize,
    const C1: usize,
    const C2: usize,
>(PhantomData<(Q1, Q2)>);

impl<
        Q1: Quantities<R, C1>,
        Q2: Quantities<R, C2>,
        const R: usize,
        const C1: usize,
        const C2: usize,
    > Quantities<R, { C1 + C2 }> for HorizontalStackQuantities<Q1, Q2, R, C1, C2>
{
    const QUANTITIES: [[SIQuantity; C1 + C2]; R] = util::transpose_arrays(util::concat(
        util::transpose_arrays(Q1::QUANTITIES),
        util::transpose_arrays(Q2::QUANTITIES),
    ));
}

pub struct VerticalStackQuantities<
    Q1: Quantities<R1, C>,
    Q2: Quantities<R2, C>,
    const R1: usize,
    const R2: usize,
    const C: usize,
>(PhantomData<(Q1, Q2)>);

impl<
        Q1: Quantities<R1, C>,
        Q2: Quantities<R2, C>,
        const R1: usize,
        const R2: usize,
        const C: usize,
    > Quantities<{ R1 + R2 }, C> for VerticalStackQuantities<Q1, Q2, R1, R2, C>
{
    const QUANTITIES: [[SIQuantity; C]; R1 + R2] = util::concat(Q1::QUANTITIES, Q2::QUANTITIES);
}

pub struct IndexQuantities<
    Q: Quantities<R, C>,
    const R: usize,
    const C: usize,
    const RS: Range<usize>,
    const CS: Range<usize>,
>(PhantomData<Q>);

impl<
        Q: Quantities<R, C>,
        const R: usize,
        const C: usize,
        const RS: Range<usize>,
        const CS: Range<usize>,
    > Quantities<{ util::range_size(RS, R) }, { util::range_size(CS, C) }>
    for IndexQuantities<Q, R, C, RS, CS>
{
    const QUANTITIES: [[SIQuantity; util::range_size(CS, C)]; util::range_size(RS, R)] =
        util::index_2d::<_, R, C, RS, CS>(Q::QUANTITIES);
}

pub trait Dimensioned {
    const ROWS: usize;
    const COLUMNS: usize;
}

#[repr(transparent)]
pub struct Value<T: ComplexField, Q: Quantities<R, C>, const R: usize, const C: usize> {
    pub value: SMatrix<T, R, C>,
    _phantom: PhantomData<Q>,
}

impl<T: ComplexField, Q: Quantities<R, C>, const R: usize, const C: usize> Value<T, Q, R, C> {
    pub const fn as_equiv<Q2: Quantities<R, C>>(&self) -> Value<T, Q2, R, C>
    where
        T: Copy,
        [(); equal_quantities::<Q, Q2, R, C>()]:,
    {
        Value {
            value: self.value,
            _phantom: PhantomData,
        }
    }

    pub const fn as_equiv_assert<Q2: Quantities<R2, C2>, const R2: usize, const C2: usize>(
        &self,
    ) -> Value<T, Q2, R2, C2>
    where
        T: Copy,
    {
        assert!(R == R2);
        assert!(C == C2);
        let mut i = 0;
        while i < R {
            let mut j = 0;
            while j < C {
                assert!(Q::QUANTITIES[i][j].is_equal(Q2::QUANTITIES[i][j]));
                j += 1;
            }
            i += 1;
        }

        union Proxy<T, const R: usize, const C: usize, const R2: usize, const C2: usize> {
            input: ManuallyDrop<ArrayStorage<T, R, C>>,
            output: ManuallyDrop<ArrayStorage<T, R2, C2>>,
        }

        Value {
            value: SMatrix::from_array_storage(unsafe {
                ManuallyDrop::into_inner(
                    Proxy {
                        input: ManuallyDrop::new(self.value.data),
                    }
                    .output,
                )
            }),
            _phantom: PhantomData,
        }
    }

    pub fn convert<U: ComplexField + SupersetOf<T>>(self) -> Value<U, Q, R, C> {
        Value {
            value: convert(self.value),
            _phantom: PhantomData,
        }
    }

    pub const fn new_si(contents: [[T; C]; R]) -> Self
    where
        T: Copy,
    {
        Self {
            value: SMatrix::from_array_storage(ArrayStorage(util::transpose_arrays(contents))),
            _phantom: PhantomData,
        }
    }

    pub const fn from_si_matrix(value: SMatrix<T, R, C>) -> Self {
        Self {
            value,
            _phantom: PhantomData,
        }
    }

    pub const fn into_si(self) -> [[T; C]; R]
    where
        T: Copy,
    {
        util::transpose_arrays(self.value.data.0)
    }

    pub const fn into_si_matrix(self) -> SMatrix<T, R, C>
    where
        T: Copy,
    {
        self.value
    }

    pub fn transpose(self) -> Value<T, TransposeQuantities<Q, R, C>, C, R> {
        Value {
            value: self.value.transpose(),
            _phantom: PhantomData,
        }
    }

    pub fn append_right<Q2: Quantities<R, C2>, const C2: usize>(
        self,
        other: Value<T, Q2, R, C2>,
    ) -> Value<T, HorizontalStackQuantities<Q, Q2, R, C, C2>, R, { C + C2 }>
    where
        T: Copy,
    {
        Value {
            value: SMatrix::from_array_storage(ArrayStorage(util::concat(
                self.value.data.0,
                other.value.data.0,
            ))),
            _phantom: PhantomData,
        }
    }

    pub fn append_below<Q2: Quantities<R2, C>, const R2: usize>(
        self,
        other: Value<T, Q2, R2, C>,
    ) -> Value<T, VerticalStackQuantities<Q, Q2, R, R2, C>, { R + R2 }, C>
    where
        T: Copy,
    {
        Value {
            value: SMatrix::from_array_storage(ArrayStorage(util::transpose_arrays(util::concat(
                util::transpose_arrays(self.value.data.0),
                util::transpose_arrays(other.value.data.0),
            )))),
            _phantom: PhantomData,
        }
    }

    pub fn index<const RS: Range<usize>, const CS: Range<usize>>(
        &self,
    ) -> Value<
        T,
        IndexQuantities<Q, R, C, RS, CS>,
        { util::range_size(RS, R) },
        { util::range_size(CS, C) },
    > {
        Value {
            value: SMatrix::from_array_storage(ArrayStorage(util::clone_from_refs_2d(
                util::index_2d::<&T, C, R, CS, RS>(util::each_ref_2d(&self.value.data.0)),
            ))),
            _phantom: PhantomData,
        }
    }

    pub fn index_assert<Q2: Quantities<R2, C2>, const R2: usize, const C2: usize>(
        &self,
        rows: Range<usize>,
        cols: Range<usize>,
    ) -> Value<T, Q2, R2, C2> {
        assert_eq!(rows.end - rows.start, R2);
        assert_eq!(cols.end - cols.start, C2);
        Value {
            value: self
                .value
                .view_range(rows, cols)
                .fixed_rows::<R2>(0)
                .fixed_columns::<C2>(0)
                .into(),
            _phantom: PhantomData,
        }
    }

    pub fn abs(self) -> Self
    where
        T: Signed,
    {
        Self {
            value: self.value.abs(),
            _phantom: PhantomData,
        }
    }
}

impl<T: ComplexField, Q: Quantities<N, N>, const N: usize> Value<T, Q, N, N>
where
    Const<N>: DimMin<Const<N>, Output = Const<N>>,
    [(); check_determinant_quantities(Q::QUANTITIES)]:,
{
    pub fn determinant(self) -> Value<T, DeterminantQuantities<Q, N>, 1, 1> {
        Value {
            value: Matrix1::new(self.value.determinant()),
            _phantom: PhantomData,
        }
    }

    pub fn try_inverse(self) -> Option<Value<T, InverseQuantities<Q, N>, N, N>>
    where
        [(); N]:,
    {
        Some(Value {
            value: self.value.try_inverse()?,
            _phantom: PhantomData,
        })
    }
}

impl<
        T: ComplexField + Copy + ~const From<f64>,
        Q: Quantities<R, C>,
        const R: usize,
        const C: usize,
    > Value<T, Q, R, C>
{
    pub const ZERO: Self = Self::new_si([[T::from(0.0); C]; R]);
}

impl<T: ComplexField, Q: Quantities<R, C>, const R: usize, const C: usize> Clone
    for Value<T, Q, R, C>
{
    fn clone(&self) -> Self {
        Self {
            value: self.value.clone(),
            _phantom: self._phantom.clone(),
        }
    }
}

impl<T: ComplexField + Copy, Q: Quantities<R, C>, const R: usize, const C: usize> Copy
    for Value<T, Q, R, C>
{
}

impl<T: ComplexField + Copy, Q: Quantities<R, C>, const R: usize, const C: usize> Debug
    for Value<T, Q, R, C>
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        util::transpose_arrays(self.value.data.0).fmt(f)
    }
}

impl<T: ComplexField + Default + Copy, Q: Quantities<R, C>, const R: usize, const C: usize> Default
    for Value<T, Q, R, C>
{
    fn default() -> Self {
        Self {
            value: SMatrix::from_array_storage(ArrayStorage(util::transpose_arrays(
                [[T::default(); C]; R],
            ))),
            _phantom: PhantomData,
        }
    }
}

impl<T: ComplexField, Q: Quantities<R, C>, const R: usize, const C: usize> PartialEq
    for Value<T, Q, R, C>
{
    fn eq(&self, other: &Self) -> bool {
        self.value == other.value
    }
}

impl<T: ComplexField + PartialOrd, Q: Quantities<R, C>, const R: usize, const C: usize> PartialOrd
    for Value<T, Q, R, C>
{
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match self.value.partial_cmp(&other.value) {
            Some(Ordering::Equal) => {}
            ord => return ord,
        }
        self._phantom.partial_cmp(&other._phantom)
    }
}

impl<
        T: ComplexField,
        U: ComplexField,
        Q1: Quantities<R, C>,
        Q2: Quantities<R, C>,
        const R: usize,
        const C: usize,
    > SubsetOf<Value<U, Q2, R, C>> for Value<T, Q1, R, C>
where
    T: SubsetOf<U>,
    [(); equal_quantities::<Q1, Q2, R, C>()]:,
{
    fn to_superset(&self) -> Value<U, Q2, R, C> {
        Value {
            value: self.value.to_superset(),
            _phantom: PhantomData,
        }
    }

    fn from_superset_unchecked(element: &Value<U, Q2, R, C>) -> Self {
        Value {
            value: SubsetOf::from_superset_unchecked(&element.value),
            _phantom: PhantomData,
        }
    }

    fn is_in_subset(element: &Value<U, Q2, R, C>) -> bool {
        <SMatrix<T, R, C> as SubsetOf<SMatrix<U, R, C>>>::is_in_subset(&element.value)
    }
}

impl<T: ComplexField, Q: Quantities<R, C>, const R: usize, const C: usize> HasUnderlyingType
    for Value<T, Q, R, C>
{
    type UnderlyingType = T;
}

impl<T: ComplexField, Q: Quantities<R, C>, const R: usize, const C: usize> HasQuantities
    for Value<T, Q, R, C>
{
    const QUANTITIES: &'static [&'static [SIQuantity]] = &util::each_row_ref_2d(&Q::QUANTITIES);
}

impl<T: ComplexField, Q: Quantities<R, C>, const R: usize, const C: usize> ValueType<R, C>
    for Value<T, Q, R, C>
{
    type Q = Q;
}

impl<T: ComplexField, Q: Quantities<R, C>, const R: usize, const C: usize> Dimensioned
    for Value<T, Q, R, C>
{
    const ROWS: usize = R;
    const COLUMNS: usize = C;
}

impl<T: ComplexField, Q: Quantities<R, C>, const R: usize, const C: usize> Neg
    for Value<T, Q, R, C>
{
    type Output = Self;

    fn neg(self) -> Self::Output {
        Self {
            value: -self.value,
            _phantom: PhantomData,
        }
    }
}

#[cfg(feature = "serde")]
#[cfg_attr(docsrs, doc(cfg(feature = "serde")))]
impl<T: ComplexField + Serialize, Q: Quantities<R, C>, const R: usize, const C: usize> Serialize
    for Value<T, Q, R, C>
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut tup = serializer.serialize_tuple(R * C)?;
        for i in 0..R {
            for j in 0..C {
                tup.serialize_element(&self.value[(i, j)])?;
            }
        }
        tup.end()
    }
}

#[cfg(feature = "serde")]
#[cfg_attr(docsrs, doc(cfg(feature = "serde")))]
impl<
        'de,
        T: ComplexField + Deserialize<'de> + Default + Copy,
        Q: Quantities<R, C>,
        const R: usize,
        const C: usize,
    > Deserialize<'de> for Value<T, Q, R, C>
{
    fn deserialize<D>(deserializer: D) -> Result<Value<T, Q, R, C>, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct ValueVisitor<
            'de,
            T: ComplexField + Deserialize<'de> + Default + Copy,
            Q: Quantities<R, C>,
            const R: usize,
            const C: usize,
        > {
            _l: PhantomData<&'de ()>,
            _t: PhantomData<T>,
            _q: PhantomData<Q>,
        }

        impl<
                'de,
                T: ComplexField + Deserialize<'de> + Default + Copy,
                Q: Quantities<R, C>,
                const R: usize,
                const C: usize,
            > Visitor<'de> for ValueVisitor<'de, T, Q, R, C>
        {
            type Value = [[T; C]; R];

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("a value")
            }

            fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
            where
                A: serde::de::SeqAccess<'de>,
            {
                let mut new_obj = [[T::default(); C]; R];
                for i in 0..R {
                    for j in 0..C {
                        match seq.next_element()? {
                            Some(value) => new_obj[i][j] = value,
                            None => return Err(serde::de::Error::invalid_length(C, &self)),
                        }
                    }
                }

                Ok(new_obj)
            }
        }

        let value = deserializer.deserialize_tuple(
            R * C,
            ValueVisitor::<'de, T, Q, R, C> {
                _l: PhantomData,
                _t: PhantomData,
                _q: PhantomData,
            },
        );
        match value {
            Ok(value) => Ok(Value::<T, Q, R, C> {
                value: SMatrix::from_array_storage(ArrayStorage(util::transpose_arrays(value))),
                _phantom: PhantomData,
            }),
            Err(err) => Err(err),
        }
    }
}

impl<
        T: ComplexField,
        Q1: Quantities<R, C>,
        Q2: Quantities<R, C>,
        const R: usize,
        const C: usize,
    > Add<Value<T, Q2, R, C>> for Value<T, Q1, R, C>
where
    [(); equal_quantities::<Q1, Q2, R, C>()]:,
{
    type Output = Self;

    fn add(self, rhs: Value<T, Q2, R, C>) -> Self::Output {
        Value {
            value: self.value + rhs.value,
            _phantom: PhantomData,
        }
    }
}

impl<
        T: ComplexField,
        Q1: Quantities<R, C>,
        Q2: Quantities<R, C>,
        const R: usize,
        const C: usize,
    > AddAssign<Value<T, Q2, R, C>> for Value<T, Q1, R, C>
where
    [(); equal_quantities::<Q1, Q2, R, C>()]:,
{
    fn add_assign(&mut self, rhs: Value<T, Q2, R, C>) {
        self.value += rhs.value;
    }
}

impl<
        T: ComplexField,
        Q1: Quantities<R, C>,
        Q2: Quantities<R, C>,
        const R: usize,
        const C: usize,
    > Sub<Value<T, Q2, R, C>> for Value<T, Q1, R, C>
where
    [(); equal_quantities::<Q1, Q2, R, C>()]:,
{
    type Output = Self;

    fn sub(self, rhs: Value<T, Q2, R, C>) -> Self::Output {
        Value {
            value: self.value - rhs.value,
            _phantom: PhantomData,
        }
    }
}

impl<
        T: ComplexField,
        Q1: Quantities<R, C>,
        Q2: Quantities<R, C>,
        const R: usize,
        const C: usize,
    > SubAssign<Value<T, Q2, R, C>> for Value<T, Q1, R, C>
where
    [(); equal_quantities::<Q1, Q2, R, C>()]:,
{
    fn sub_assign(&mut self, rhs: Value<T, Q2, R, C>) {
        self.value -= rhs.value;
    }
}

impl<
        T: ComplexField,
        Q1: Quantities<R1, C1>,
        Q2: Quantities<R2, C2>,
        const R1: usize,
        const C1: usize,
        const R2: usize,
        const C2: usize,
    > Mul<Value<T, Q2, R2, C2>> for Value<T, Q1, R1, C1>
where
    MulQuantities<Q1, Q2, R1, C1, R2, C2>:
        Quantities<{ mul_rows(R1, C1, R2, C2) }, { mul_cols(R1, C1, R2, C2) }>,
{
    type Output = Value<
        T,
        MulQuantities<Q1, Q2, R1, C1, R2, C2>,
        { mul_rows(R1, C1, R2, C2) },
        { mul_cols(R1, C1, R2, C2) },
    >;

    fn mul(self, rhs: Value<T, Q2, R2, C2>) -> Self::Output {
        Value {
            value: if R1 == 1 && C1 == 1 {
                util::cast::<
                    _,
                    SMatrix<T, { mul_rows(R1, C1, R2, C2) }, { mul_cols(R1, C1, R2, C2) }>,
                >(rhs.value)
                    * self.value[0].clone()
            } else if R2 == 1 && C2 == 1 {
                util::cast::<
                    _,
                    SMatrix<T, { mul_rows(R1, C1, R2, C2) }, { mul_cols(R1, C1, R2, C2) }>,
                >(self.value)
                    * rhs.value[0].clone()
            } else {
                let (lhs, rhs) = util::cast::<
                    _,
                    (
                        SMatrix<T, { mul_rows(R1, C1, R2, C2) }, C1>,
                        SMatrix<T, C1, { mul_cols(R1, C1, R2, C2) }>,
                    ),
                >((self.value, rhs.value));
                lhs * rhs
            },
            _phantom: PhantomData,
        }
    }
}

const fn check_mul_assign<
    Q1: Quantities<R1, C1>,
    Q2: Quantities<R2, C2>,
    const R1: usize,
    const C1: usize,
    const R2: usize,
    const C2: usize,
>() -> usize {
    if R2 == 1 && C2 == 1 {
        assert!(Q2::QUANTITIES[0][0].is_equal(RATIO));
    } else {
        assert!(R2 == C1);
        assert!(C2 == C1);
        let mut i = 0;
        while i < C1 {
            let mut j = 0;
            while j < C1 {
                assert!(Q2::QUANTITIES[i][j].is_equal(RATIO));
                j += 1;
            }
            i += 1;
        }
    }
    0
}

impl<
        T: ComplexField,
        Q1: Quantities<R1, C1>,
        Q2: Quantities<R2, C2>,
        const R1: usize,
        const C1: usize,
        const R2: usize,
        const C2: usize,
    > MulAssign<Value<T, Q2, R2, C2>> for Value<T, Q1, R1, C1>
where
    MulQuantities<Q1, Q2, R1, C1, R2, C2>:
        Quantities<{ mul_rows(R1, C1, R2, C2) }, { mul_cols(R1, C1, R2, C2) }>,
    [(); check_mul_assign::<Q1, Q2, R1, C1, R2, C2>()]:,
{
    fn mul_assign(&mut self, rhs: Value<T, Q2, R2, C2>) {
        if R2 == 1 && C2 == 1 {
            self.value *= rhs.value[0].clone();
        } else {
            let rhs = util::cast::<_, SMatrix<T, C1, C1>>(rhs.value);
            self.value *= rhs;
        }
    }
}

impl<T: ComplexField, Q: Quantities<R, C>, const R: usize, const C: usize> Mul<T>
    for Value<T, Q, R, C>
{
    type Output = Self;

    fn mul(self, rhs: T) -> Self::Output {
        Self {
            value: self.value * rhs,
            _phantom: PhantomData,
        }
    }
}

impl<T: ComplexField, Q: Quantities<R, C>, const R: usize, const C: usize> MulAssign<T>
    for Value<T, Q, R, C>
{
    fn mul_assign(&mut self, rhs: T) {
        self.value *= rhs;
    }
}

impl<
        T: ComplexField,
        Q1: Quantities<R, C>,
        Q2: Quantities<1, 1>,
        const R: usize,
        const C: usize,
    > Div<Value<T, Q2, 1, 1>> for Value<T, Q1, R, C>
{
    type Output = Value<T, DivQuantities<Q1, Q2, R, C>, R, C>;

    fn div(self, rhs: Value<T, Q2, 1, 1>) -> Self::Output {
        Value {
            value: self.value / rhs.value[0].clone(),
            _phantom: PhantomData,
        }
    }
}

impl<
        T: ComplexField,
        Q1: Quantities<R, C>,
        Q2: Quantities<1, 1>,
        const R: usize,
        const C: usize,
    > DivAssign<Value<T, Q2, 1, 1>> for Value<T, Q1, R, C>
{
    fn div_assign(&mut self, rhs: Value<T, Q2, 1, 1>) {
        self.value /= rhs.value[0].clone();
    }
}

impl<T: ComplexField, Q: Quantities<R, C>, const R: usize, const C: usize> Div<T>
    for Value<T, Q, R, C>
{
    type Output = Self;

    fn div(self, rhs: T) -> Self::Output {
        Self {
            value: self.value / rhs,
            _phantom: PhantomData,
        }
    }
}

impl<T: ComplexField, Q: Quantities<R, C>, const R: usize, const C: usize> DivAssign<T>
    for Value<T, Q, R, C>
{
    fn div_assign(&mut self, rhs: T) {
        self.value /= rhs;
    }
}

impl<
        T: ComplexField + Copy + ~const From<f64>,
        Q: Quantities<R, C>,
        const R: usize,
        const C: usize,
    > Sum for Value<T, Q, R, C>
{
    fn sum<I: Iterator<Item = Self>>(iter: I) -> Self {
        Self {
            value: iter.map(|v| v.value).sum(),
            _phantom: PhantomData,
        }
    }
}

impl<T: ComplexField + Zero + Copy, Q: Quantities<R, C>, const R: usize, const C: usize> Zero
    for Value<T, Q, R, C>
where
    [(); equal_quantities::<Q, Q, R, C>()]:,
{
    fn zero() -> Self {
        Self::new_si([[T::zero(); C]; R])
    }

    fn is_zero(&self) -> bool {
        self.value.is_zero()
    }
}

#![cfg(feature = "coordinate-systems")]
#![cfg_attr(docsrs, doc(cfg(feature = "coordinate-systems")))]

use nalgebra::RealField;

use crate::{angle::Angle, matrix, SIQuantity, UniformQuantities, Value};

/// Represents polar coordinates in 2D with physical units.
pub struct Polar<T: RealField, const Q: SIQuantity> {
    /// The radial coordinate.
    pub radius: Value<T, UniformQuantities<Q, 1, 1>, 1, 1>,
    /// The angular coordinate.
    pub azimuth: Angle<T>,
}

impl<T: RealField + Copy, const Q: SIQuantity> Polar<T, Q> {
    pub fn to_cartesian(self) -> Value<T, UniformQuantities<Q, 2, 1>, 2, 1> {
        matrix![
            self.radius * self.azimuth.cos().value[0];
            self.radius * self.azimuth.sin().value[0]
        ]
        .as_equiv_assert()
    }
}

impl<T: RealField + Copy, const Q: SIQuantity> Value<T, UniformQuantities<Q, 2, 1>, 2, 1> {
    pub fn to_polar(self) -> Polar<T, Q> {
        Polar {
            radius: self.norm(),
            azimuth: Value::new_si([[self.value[1].atan2(self.value[0])]]),
        }
    }
}

/// Represents cylindrical coordinates in 3D with physical units.
pub struct Cylindrical<T: RealField, const Q: SIQuantity> {
    /// The radial and axial coordinates.
    pub coordinate: Value<T, UniformQuantities<Q, 2, 1>, 2, 1>,
    /// The angular coordinate.
    pub azimuth: Angle<T>,
}

impl<T: RealField + Copy, const Q: SIQuantity> Cylindrical<T, Q> {
    pub fn to_cartesian(self) -> Value<T, UniformQuantities<Q, 3, 1>, 3, 1> {
        #[qunit_macros::matrix_pat]
        let matrix![
            radius: Value<T, UniformQuantities<Q, 1, 1>, 1, 1>;
            z: Value<T, UniformQuantities<Q, 1, 1>, 1, 1>
        ] = self.coordinate;
        matrix![Polar { radius, azimuth: self.azimuth }.to_cartesian(); z].as_equiv_assert()
    }
}

/// Represents spherical coordinates in 3D with physical units.
pub struct Spherical<T: RealField, const Q: SIQuantity> {
    /// The radial coordinate.
    pub radius: Value<T, UniformQuantities<Q, 1, 1>, 1, 1>,
    /// The azimuth and elevation.
    pub angles: Angle<T, 2>,
}

impl<T: RealField + Copy, const Q: SIQuantity> Spherical<T, Q> {
    pub fn to_cartesian(self) -> Value<T, UniformQuantities<Q, 3, 1>, 3, 1> {
        #[qunit_macros::matrix_pat]
        let matrix![azimuth; elevation] = self.angles;
        Cylindrical {
            coordinate: Polar {
                radius: self.radius,
                azimuth: elevation,
            }
            .to_cartesian(),
            azimuth,
        }
        .to_cartesian()
    }
}

impl<T: RealField + Copy, const Q: SIQuantity> Value<T, UniformQuantities<Q, 3, 1>, 3, 1> {
    pub fn to_cylindrical(self) -> Cylindrical<T, Q> {
        #[qunit_macros::matrix_pat]
        let matrix![
            xy: Value<T, UniformQuantities<Q, 2, 1>, 2, 1>;
            z: Value<T, UniformQuantities<Q, 1, 1>, 1, 1>
        ] = self;
        let Polar { radius, azimuth } = xy.to_polar();
        Cylindrical {
            coordinate: matrix![radius; z].as_equiv_assert(),
            azimuth,
        }
    }

    pub fn to_spherical(self) -> Spherical<T, Q> {
        let Cylindrical {
            coordinate,
            azimuth,
        } = self.to_cylindrical();
        let Polar {
            radius,
            azimuth: elevation,
        } = coordinate.to_polar();
        Spherical {
            radius,
            angles: matrix![azimuth; elevation].as_equiv_assert(),
        }
    }
}

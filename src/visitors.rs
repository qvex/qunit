#![cfg(feature = "visitors")]
#![cfg_attr(docsrs, doc(cfg(feature = "visitors")))]

use core::{mem::MaybeUninit, num::NonZeroUsize};

use nalgebra::ComplexField;

use crate::{Quantities, Value};

impl<T: ComplexField + Copy, Q: Quantities<R, C>, const R: usize, const C: usize>
    Value<T, Q, R, C>
{
    pub fn visit(&self) -> impl Visitor<Field = T> + '_ {
        MatrixVisitor {
            value: self,
            cursor: Cursor::RowStart { row: 0 },
        }
    }
}

pub trait Visitor {
    type Field: ComplexField;

    fn visit<Q: Quantities<R, C>, const R: usize, const C: usize>(
        &mut self,
    ) -> Value<Self::Field, Q, R, C>;

    fn eol(&mut self);

    fn eof(&mut self);
}

struct MatrixVisitor<'a, T: ComplexField, Q: Quantities<R, C>, const R: usize, const C: usize> {
    value: &'a Value<T, Q, R, C>,
    cursor: Cursor,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
enum Cursor {
    RowStart {
        row: usize,
    },
    Middle {
        row: usize,
        col: NonZeroUsize,
        height: NonZeroUsize,
    },
}

impl Cursor {
    fn check_bounds(self, rows: usize, cols: usize, height: usize, width: usize) -> bool {
        match self {
            Self::RowStart { row } => row + height <= rows,
            Self::Middle {
                row: _, // Row has already been checked against this height.
                col,
                height: cur_height,
            } => {
                height == Into::<usize>::into(cur_height)
                    && Into::<usize>::into(col) + width <= cols
            }
        }
    }

    fn to_pair(self) -> (usize, usize) {
        match self {
            Self::RowStart { row } => (row, 0),
            Self::Middle {
                row,
                col,
                height: _,
            } => (row, col.into()),
        }
    }
}

impl<'a, T: ComplexField + Copy, Q: Quantities<R, C>, const R: usize, const C: usize> Visitor
    for MatrixVisitor<'a, T, Q, R, C>
{
    type Field = T;

    fn visit<Q2: Quantities<R2, C2>, const R2: usize, const C2: usize>(
        &mut self,
    ) -> Value<Self::Field, Q2, R2, C2> {
        assert!(self.cursor.check_bounds(R, C, R2, C2));
        let mut result = MaybeUninit::uninit_array();
        let (base_i, base_j) = self.cursor.to_pair();
        let mut i = 0;
        while i < R2 {
            let mut row = MaybeUninit::uninit_array();
            let mut j = 0;
            while j < C2 {
                row[j] = MaybeUninit::new(self.value.value[(base_i + i, base_j + j)]);
                j += 1;
            }
            result[i] = row.transpose();
            i += 1;
        }
        self.cursor = Cursor::Middle {
            row: base_i,
            col: (base_j + C2).try_into().unwrap(),
            height: R2.try_into().unwrap(),
        };
        Value::new_si(unsafe { result.transpose().assume_init() })
    }

    fn eol(&mut self) {
        self.cursor = match self.cursor {
            Cursor::RowStart { row: _ } => {
                panic!("not at end of row")
            }
            Cursor::Middle { row, col, height } => {
                assert_eq!(Into::<usize>::into(col), C, "not at end of row");
                Cursor::RowStart {
                    row: row + Into::<usize>::into(height),
                }
            }
        }
    }

    fn eof(&mut self) {
        assert_eq!(
            self.cursor,
            Cursor::RowStart { row: R },
            "not at end of matrix"
        );
    }
}

#![cfg(all(feature = "math", any(feature = "libm", feature = "std")))]
#![cfg_attr(
    docsrs,
    doc(cfg(all(feature = "math", any(feature = "libm", feature = "std"))))
)]

use core::marker::PhantomData;
use nalgebra::{ArrayStorage, ComplexField, RealField, SMatrix};

use crate::{Quantities, SIQuantity, UniformQuantities, Value, RATIO};

impl<T: ComplexField, Q: Quantities<R, C>, const R: usize, const C: usize> Value<T, Q, R, C> {
    pub(crate) fn element_wise<U: ComplexField, Q2: Quantities<R, C>>(
        self,
        f: impl Fn(T) -> U,
    ) -> Value<U, Q2, R, C> {
        Value {
            value: SMatrix::from_array_storage(ArrayStorage(
                self.value.data.0.map(|col| col.map(|x| f(x))),
            )),
            _phantom: PhantomData,
        }
    }

    pub(crate) fn element_wise_zip<
        U: ComplexField,
        V: ComplexField,
        Q2: Quantities<R, C>,
        Q3: Quantities<R, C>,
    >(
        self,
        rhs: Value<U, Q2, R, C>,
        f: impl Fn(T, U) -> V,
    ) -> Value<V, Q3, R, C> {
        Value {
            value: SMatrix::from_array_storage(ArrayStorage(
                self.value
                    .data
                    .0
                    .zip(rhs.value.data.0)
                    .map(|(lhs, rhs)| lhs.zip(rhs).map(|(x, y)| f(x, y))),
            )),
            _phantom: PhantomData,
        }
    }
}

impl<T: RealField, Q: Quantities<R, C>, const R: usize, const C: usize> Value<T, Q, R, C> {
    pub fn atan2(self, x: Self) -> crate::ratio::Ratio<T, R, C> {
        self.element_wise_zip(x, T::atan2)
    }
}

impl<T: ComplexField, const Q: SIQuantity, const R: usize>
    Value<T, UniformQuantities<Q, R, 1>, R, 1>
{
    pub fn norm(self) -> Value<T::RealField, UniformQuantities<Q, 1, 1>, 1, 1>
    where
        <T as ComplexField>::RealField: Copy,
    {
        Value::new_si([[self.value.norm()]])
    }
}

pub struct SqrtQuantities<Q: Quantities<R, C>, const R: usize, const C: usize>(PhantomData<Q>);

impl<Q: Quantities<R, C>, const R: usize, const C: usize> Quantities<R, C>
    for SqrtQuantities<Q, R, C>
{
    const QUANTITIES: [[SIQuantity; C]; R] = {
        let mut result = [[RATIO; C]; R];
        let mut i = 0;
        while i < R {
            let mut j = 0;
            while j < C {
                result[i][j] = Q::QUANTITIES[i][j].sqrt();
                j += 1;
            }
            i += 1;
        }
        result
    };
}

impl<T: RealField, Q: Quantities<R, C>, const R: usize, const C: usize> Value<T, Q, R, C> {
    pub fn sqrt(self) -> Value<T, SqrtQuantities<Q, R, C>, R, C> {
        self.element_wise(T::sqrt)
    }
}

pub struct MatrixSqrtQuantities<Q: Quantities<N, N>, const N: usize>(PhantomData<Q>);

impl<Q: Quantities<N, N>, const N: usize> Quantities<N, N> for MatrixSqrtQuantities<Q, N> {
    const QUANTITIES: [[SIQuantity; N]; N] = {
        let mut result = [crate::RATIO; N];
        let mut i = 0;
        while i < N {
            result[i] = Q::QUANTITIES[i][i].sqrt();
            let mut j = 0;
            while j < N {
                assert!(Q::QUANTITIES[i][i]
                    .mul(Q::QUANTITIES[j][j])
                    .sqrt()
                    .is_equal(Q::QUANTITIES[i][j]));
                j += 1;
            }
            i += 1;
        }
        [result; N]
    };
}

impl<T: ComplexField, Q: Quantities<N, N>, const N: usize> Value<T, Q, N, N>
where
    MatrixSqrtQuantities<Q, N>: Quantities<N, N>,
{
    pub fn matrix_sqrt(self) -> Option<Value<T, MatrixSqrtQuantities<Q, N>, N, N>> {
        Some(Value {
            value: self.value.cholesky()?.l().adjoint(),
            _phantom: PhantomData,
        })
    }
}

impl<T: ComplexField, const R: usize, const C: usize> crate::ratio::Ratio<T, R, C> {
    pub fn sin(self) -> Self {
        self.element_wise(T::sin)
    }

    pub fn cos(self) -> Self {
        self.element_wise(T::cos)
    }

    pub fn tan(self) -> Self {
        self.element_wise(T::tan)
    }

    pub fn asin(self) -> Self {
        self.element_wise(T::asin)
    }

    pub fn acos(self) -> Self {
        self.element_wise(T::acos)
    }

    pub fn atan(self) -> Self {
        self.element_wise(T::atan)
    }
}

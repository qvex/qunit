use core::{
    any::TypeId,
    mem::{ManuallyDrop, MaybeUninit},
    ops::Range,
};

pub const fn cast<T: 'static, U: 'static>(value: T) -> U {
    assert!(TypeId::of::<T>() == TypeId::of::<U>());

    union Data<T, U> {
        fst: ManuallyDrop<T>,
        snd: ManuallyDrop<U>,
    }

    ManuallyDrop::into_inner(unsafe {
        Data {
            fst: ManuallyDrop::new(value),
        }
        .snd
    })
}

pub const fn idiv(lhs: isize, rhs: isize) -> isize {
    assert!(lhs % rhs == 0);
    lhs / rhs
}

pub const fn transpose_arrays<T: Copy, const R: usize, const C: usize>(
    array: [[T; C]; R],
) -> [[T; R]; C] {
    let mut result = MaybeUninit::uninit_array();
    let mut i = 0;
    while i < C {
        let mut row = MaybeUninit::uninit_array();
        let mut j = 0;
        while j < R {
            row[j] = MaybeUninit::new(array[j][i]);
            j += 1;
        }
        result[i] = row.transpose();
        i += 1;
    }
    unsafe { result.transpose().assume_init() }
}

pub const fn each_ref<T, const N: usize>(array: &[T; N]) -> [&T; N] {
    let mut result = [MaybeUninit::uninit(); N];
    let mut i = 0;
    while i < N {
        result[i] = MaybeUninit::new(&array[i]);
        i += 1;
    }
    unsafe { result.transpose().assume_init() }
}

pub const fn each_ref_2d<T, const R: usize, const C: usize>(array: &[[T; C]; R]) -> [[&T; C]; R] {
    let mut result = [MaybeUninit::uninit(); R];
    let mut i = 0;
    while i < R {
        result[i] = MaybeUninit::new(each_ref(&array[i]));
        i += 1;
    }
    unsafe { result.transpose().assume_init() }
}

pub const fn each_row_ref_2d<T, const R: usize, const C: usize>(array: &[[T; C]; R]) -> [&[T]; R] {
    let rows = each_ref(array);
    let mut result = [MaybeUninit::uninit(); R];
    let mut i = 0;
    while i < R {
        result[i] = MaybeUninit::new(rows[i] as &[T]);
        i += 1;
    }
    unsafe { result.transpose().assume_init() }
}

pub const fn concat<T: Copy, const M: usize, const N: usize>(
    fst: [T; M],
    snd: [T; N],
) -> [T; M + N] {
    let mut result = [MaybeUninit::uninit(); M + N];
    let mut i = 0;
    while i < M {
        result[i] = MaybeUninit::new(fst[i]);
        i += 1;
    }
    i = 0;
    while i < N {
        result[M + i] = MaybeUninit::new(snd[i]);
        i += 1;
    }
    unsafe { result.transpose().assume_init() }
}

pub const fn from_refs_2d<T: Copy, const R: usize, const C: usize>(array: &[&[T]]) -> [[T; C]; R] {
    let mut result = MaybeUninit::uninit_array();
    let mut i = 0;
    while i < R {
        let mut row = MaybeUninit::uninit_array();
        let mut j = 0;
        while j < R {
            row[j] = MaybeUninit::new(array[i][j]);
            j += 1;
        }
        result[i] = row.transpose();
        i += 1;
    }
    unsafe { result.transpose().assume_init() }
}

pub const fn range_size(range: Range<usize>, size: usize) -> usize {
    assert!(range.end <= size);
    range.end - range.start
}

pub const fn index_2d<
    T: Copy,
    const R: usize,
    const C: usize,
    const RS: Range<usize>,
    const CS: Range<usize>,
>(
    array: [[T; C]; R],
) -> [[T; range_size(CS, C)]; range_size(RS, R)] {
    let mut result = [MaybeUninit::uninit(); range_size(RS, R)];
    let mut i = RS.start;
    while i < RS.end {
        let mut row = [MaybeUninit::uninit(); range_size(CS, C)];
        let mut j = CS.start;
        while j < CS.end {
            row[j - CS.start] = MaybeUninit::new(array[i][j]);
            j += 1;
        }
        result[i - RS.start] = row.transpose();
        i += 1;
    }
    unsafe { result.transpose().assume_init() }
}

pub fn clone_from_refs_2d<T: Clone, const R: usize, const C: usize>(
    array: [[&T; C]; R],
) -> [[T; C]; R] {
    let mut result = MaybeUninit::uninit_array();
    let mut i = 0;
    while i < R {
        let mut row = MaybeUninit::uninit_array();
        let mut j = 0;
        while j < R {
            row[j] = MaybeUninit::new(array[i][j].clone());
            j += 1;
        }
        result[i] = row.transpose();
        i += 1;
    }
    unsafe { result.transpose().assume_init() }
}
